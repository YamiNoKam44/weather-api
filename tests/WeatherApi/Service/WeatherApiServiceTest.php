<?php
declare(strict_types=1);


namespace App\Tests\WeatherApi\Service;


use App\WeatherApi\Domain\ValueObject\Temperature;
use App\WeatherApi\Exception\WeatherApiException;
use App\WeatherApi\Infrastructure\Repository\OpenWeatherMapApiRepository;
use App\WeatherApi\Infrastructure\Repository\WeatherApiRepository;
use App\WeatherApi\Infrastructure\Repository\WeatherApiRepositoryInterface;
use App\WeatherApi\Service\WeatherApiService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class WeatherApiServiceTest extends TestCase
{
    /** @var OpenWeatherMapApiRepository|\PHPUnit\Framework\MockObject\MockObject */
    private WeatherApiRepositoryInterface $openWeatherMapApiRepository;

    /** @var WeatherApiRepository|\PHPUnit\Framework\MockObject\MockObject */
    private WeatherApiRepositoryInterface $weatherApiRepository;

    protected function setUp(): void {
        $this->openWeatherMapApiRepository = $this->createMock(OpenWeatherMapApiRepository::class);
        $this->weatherApiRepository = $this->createMock(WeatherApiRepository::class);
    }

    public function testIsClassInstatiable() {
        self::assertInstanceOf(WeatherApiService::class,
            new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository));
    }

    public function testServiceWillThrowNotFoundExcpetion() {
        $this->openWeatherMapApiRepository->method('fetchByCityAndCountryName')
            ->willThrowException(new HttpException(Response::HTTP_NOT_FOUND));

        $this->expectException(WeatherApiException::class);
        $this->expectExceptionMessage('Wrong city name or incorrect country');

        $weatherApiService = new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository);
        $weatherApiService->fetchAndCalculateTemperature('test', 'tt');
    }

    public function testServiceWillThrowUnauthorizedExcpetion() {
        $this->openWeatherMapApiRepository->method('fetchByCityAndCountryName')
            ->willThrowException(new HttpException(Response::HTTP_UNAUTHORIZED));

        $this->expectException(WeatherApiException::class);
        $this->expectExceptionMessage('Wrong api key, check it or contact with page administrator');

        $weatherApiService = new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository);
        $weatherApiService->fetchAndCalculateTemperature('test', 'tt');
    }

    public function testServiceWillThrowRequestLimitExcpetion() {
        $this->openWeatherMapApiRepository->method('fetchByCityAndCountryName')
            ->willThrowException(new HttpException(Response::HTTP_TOO_MANY_REQUESTS));

        $this->expectException(WeatherApiException::class);
        $this->expectExceptionMessage('Request limit');

        $weatherApiService = new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository);
        $weatherApiService->fetchAndCalculateTemperature('test', 'tt');
    }

    public function testServiceWillThrowUnknowExcpetion() {
        $this->openWeatherMapApiRepository->method('fetchByCityAndCountryName')
            ->willThrowException(new HttpException(Response::HTTP_I_AM_A_TEAPOT));

        $this->expectException(WeatherApiException::class);
        $this->expectExceptionMessage('Unknow Error');

        $weatherApiService = new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository);
        $weatherApiService->fetchAndCalculateTemperature('test', 'tt');
    }

    public function testServiceWillReturnTemperature() {
        $this->openWeatherMapApiRepository->method('fetchByCityAndCountryName')
            ->willReturn(300.0);
        $this->weatherApiRepository->method('fetchByCityName')
            ->willReturn(20.5);

        $weatherApiService = new WeatherApiService($this->openWeatherMapApiRepository, $this->weatherApiRepository);
        $result = $weatherApiService->fetchAndCalculateTemperature('test', 'tt');

        self::assertInstanceOf(Temperature::class, $result);
        self::assertEquals(23.7, $result->getTemperature());
    }
}