<?php
declare(strict_types=1);

namespace App\Tests\WeatherApi\Domain\ValueObject;

use App\WeatherApi\Domain\ValueObject\Temperature;
use PHPUnit\Framework\TestCase;

class TemperatureTest extends TestCase
{
    public function testIsClassInstatiable() {
        self::assertInstanceOf(Temperature::class, new Temperature(1.4));
    }

    public function testTemperatureFor4Temperatures() {
        $temperatureObject = new Temperature(4, 10, 22, 50);

        self::assertEquals(21.5, $temperatureObject->getTemperature());
        self::assertEquals('{"temperature":21.5}', $temperatureObject->toJson());
    }

    public function testChangeTemperatureFromKelvinToCelcius() {
        self::assertEquals(26.9, Temperature::calculateFromKelvinToCelsius(300));
    }
}