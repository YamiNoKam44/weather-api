<?php
declare(strict_types=1);


namespace App\Tests\WeatherApi\Domain\ValueObject\WeatherApi;


use App\WeatherApi\Domain\ValueObject\WeatherApi\RequestQueryBuilder;
use PHPUnit\Framework\TestCase;

class RequestQueryBuilderTest extends TestCase
{
    public function testIsClassInstatiable() {
        self::assertInstanceOf(RequestQueryBuilder::class, new RequestQueryBuilder());
    }

    public function testBuildQueryForCity() {
        self::assertEquals('q=a&key=b', RequestQueryBuilder::buildByCityQuery('a',  'b'));
    }
}