<?php
declare(strict_types=1);


namespace App\Tests\WeatherApi\Domain\ValueObject\OpenWeatherMapApi;


use App\WeatherApi\Domain\ValueObject\OpenWeatherMapApi\RequestQueryBuilder;
use PHPUnit\Framework\TestCase;

class RequestQueryBuilderTest extends TestCase
{
    public function testIsClassInstatiable() {
        self::assertInstanceOf(RequestQueryBuilder::class, new RequestQueryBuilder());
    }

    public function testBuildQueryForCityAndCountry() {
        self::assertEquals('q=a,a&appid=b', RequestQueryBuilder::buildByCityAndCountryQuery('a', 'a', 'b'));
    }

    public function testBuildQueryForCity() {
        self::assertEquals('q=a&appid=b', RequestQueryBuilder::buildByCityQuery('a',  'b'));
    }
}