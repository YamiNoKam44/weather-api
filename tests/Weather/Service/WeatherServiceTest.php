<?php
declare(strict_types=1);


namespace App\Tests\Weather\Service;


use App\Weather\Infrastructure\Repository\WeatherRepositoryInterface;
use App\Weather\Model\Weather;
use App\Weather\Service\WeatherService;
use App\WeatherApi\Domain\ValueObject\Temperature;
use App\WeatherApi\Service\WeatherApiService;
use PHPUnit\Framework\TestCase;

class WeatherServiceTest extends TestCase
{
    /** @var WeatherApiService|\PHPUnit\Framework\MockObject\MockObject */
    private WeatherApiService $weatherApiService;
    /** @var WeatherRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject */
    private WeatherRepositoryInterface $weatherRepository;

    protected function setUp(): void {
        $this->weatherApiService = $this->createMock(WeatherApiService::class);
        $this->weatherRepository = $this->createMock(WeatherRepositoryInterface::class);
    }

    public function testIsClassInstatiable() {
        self::assertInstanceOf(WeatherService::class,
            new WeatherService($this->weatherApiService, $this->weatherRepository));
    }

    public function testFeatchingTemperature() {
        $this->weatherApiService->method('fetchAndCalculateTemperature')->willReturn(new Temperature(2, 2));

        $weatherService = new WeatherService($this->weatherApiService, $this->weatherRepository);
        $result = $weatherService->fetchAndStoreDataByCityAndCountry('test', 'test');

        self::assertInstanceOf(Temperature::class, $result);
        self::assertEquals(2, $result->getTemperature());
    }
}