**Weather Temperature Service**

---

## Configuration Env file

1. Copy file `.env.dist` and paste as `.env` file
2. Add api keys to `.env` file (you may take it from websites writed up to places for key)
3. Configure correct `env` and `DATABASE_URL` if needed to change

---

## Configuration by docker
Before you started - install docker (docker desktop/docker compose) on your computer, configure .env file and now

1. Run on comand line on project `docker-compose -f docker-compose.yml up` or just turn on that docker images
2. Connect to php-fpm image cli to run command `composer install`
3. Don't leave that cli, and make migrate with database `php bin/console doctrine:database:create`
4. And after that migration `php bin/console doctrine:migrations:migrate`
5. Now If you go to `http://localhost:8344/` and it works!

---

## Add additional apis

If you want to add additional api to make temperature:

1. First add your new api implementation into `src/WeatherApi/Infrastructure/Repository` with implement interface `WeatherApiRepositoryInterface`
2. Add to services `config/services` new service as api repository and new parameter to `ApiWeatherService` in the same way as else apis
3. Next modify `src/WeatherApi/Service/WeatherApiService` by Adding new repository as new api [in same way as else repository by calling it in try-catch] 
4. Result of featch/get temperature add to return of `fetchAndCalculateTemperature` function (and convert it, if needed)
5. And that's, if there's not any exception - it may works :)
