<?php
declare(strict_types=1);

namespace App\Weather\Infrastructure\Repository;

use App\Weather\Model\Weather;
use Doctrine\DBAL\Driver\Connection;

class WeatherRepository implements WeatherRepositoryInterface
{
    private Connection $connection;

    /**
     * WeatherRepository constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    public function store(Weather $weather): void {
        $insert = /** @lang MySQL */
            '
            INSERT INTO weather
            (create_data, city, country, temperature) 
            VALUE 
            (:date, :city, :countryCode, :temperature)';
        $statement = $this->connection->prepare($insert);

        $statement->execute([
            'date' => $weather->getCreateData()->format('Y-m-d H:i:s'),
            'city' => $weather->getCity(),
            'countryCode' => $weather->getCountry(),
            'temperature' => $weather->getTemperature()
        ]);
    }
}