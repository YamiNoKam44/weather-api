<?php
declare(strict_types=1);

namespace App\Weather\Infrastructure\Repository;


use App\Weather\Model\Weather;

interface WeatherRepositoryInterface
{
    public function store(Weather $weather): void;
}