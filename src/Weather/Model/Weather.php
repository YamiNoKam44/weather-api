<?php
declare(strict_types=1);

namespace App\Weather\Model;

use Doctrine\ORM\Mapping as ORM;
use App\Weather\Infrastructure\Repository\WeatherRepository;

/**
 * Class Weather
 * @package App\Domain\Weather\Model
 * @ORM\Entity(repositoryClass=WeatherRepository::class)
 *
 */
class Weather
{
    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $country;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=1)
     */
    private float $temperature;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @ORM\Id
     */
    private \DateTimeImmutable $createData;

    /**
     * Weather constructor.
     * @param string $city
     * @param float $temperature
     * @param \DateTimeImmutable $createData
     */
    public function __construct(string $city, string $country, float $temperature, \DateTimeImmutable $createData) {
        $this->city = $city;
        $this->country = $country;
        $this->temperature = $temperature;
        $this->createData = $createData;
    }

    /**
     * @return string
     */
    public function getCity(): string {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string {
        return $this->country;
    }

    /**
     * @return float
     */
    public function getTemperature(): float {
        return $this->temperature;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateData(): \DateTimeImmutable {
        return $this->createData;
    }
}