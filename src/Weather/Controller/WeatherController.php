<?php
declare(strict_types=1);

namespace App\Weather\Controller;

use App\Weather\Service\WeatherService;
use App\WeatherApi\Domain\ValueObject\Temperature;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class WeatherController extends AbstractController
{

    public function index(): Response {
        return new Response($this->renderView('Weather/form.html.twig'));
    }

    public function featchWeatherByCityName(string $city, string $country, WeatherService $weatherService) {
        $result = $weatherService->fetchAndStoreDataByCityAndCountry($city, $country);

        $response = new JsonResponse($result->toArray());
        $response->setMaxAge(60 * 30);
        return $response;
    }
}