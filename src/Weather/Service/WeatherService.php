<?php
declare(strict_types=1);


namespace App\Weather\Service;

use App\Weather\DTO\WeatherResultDTO;
use App\Weather\Infrastructure\Repository\WeatherRepositoryInterface;
use App\Weather\Model\Weather;
use App\WeatherApi\Domain\ValueObject\Temperature;
use App\WeatherApi\Service\WeatherApiService;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class WeatherService
{
    private WeatherApiService $weatherApiService;
    private WeatherRepositoryInterface $weatherRepository;

    /**
     * WeatherService constructor.
     * @param WeatherApiService $weatherApiService
     * @param WeatherRepositoryInterface $weatherRepository
     */
    public function __construct(WeatherApiService $weatherApiService, WeatherRepositoryInterface $weatherRepository) {
        $this->weatherApiService = $weatherApiService;
        $this->weatherRepository = $weatherRepository;
    }

    /**
     * @param string $city
     * @param string $country
     * @return Temperature
     */
    public function fetchAndStoreDataByCityAndCountry(string $city, string $country): Temperature {
        $temperature = $this->weatherApiService->fetchAndCalculateTemperature($city, $country);

        $weather = new Weather($city, $country, $temperature->getTemperature(), new \DateTimeImmutable());
        $this->weatherRepository->store($weather);

        return $temperature;
    }
}