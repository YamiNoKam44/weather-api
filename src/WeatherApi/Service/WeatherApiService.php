<?php
declare(strict_types=1);

namespace App\WeatherApi\Service;


use App\WeatherApi\Exception\WeatherApiException;
use App\WeatherApi\Infrastructure\Repository\WeatherApiRepositoryInterface;
use App\WeatherApi\Domain\ValueObject\Temperature;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class WeatherApiService
{
    private WeatherApiRepositoryInterface $openWeatherMapApiRepository;
    private WeatherApiRepositoryInterface $weatherApiRepository;

    /**
     * WeatherApiService constructor.
     * @param WeatherApiRepositoryInterface $weatherApiRepository
     */
    public function __construct(WeatherApiRepositoryInterface $openWeatherMapApiRepository,
                                WeatherApiRepositoryInterface $weatherApiRepository) {
        $this->openWeatherMapApiRepository = $openWeatherMapApiRepository;
        $this->weatherApiRepository = $weatherApiRepository;
    }

    public function fetchAndCalculateTemperature(string $city, string $country) {
        try {
            $openWeatherMapTemperature = $this->openWeatherMapApiRepository->fetchByCityAndCountryName($city, $country);
            $weatherTemperature = $this->weatherApiRepository->fetchByCityName($city);
        } catch (HttpExceptionInterface $exception) {
            throw new WeatherApiException($exception->getStatusCode());
        }

        return new Temperature(Temperature::calculateFromKelvinToCelsius($openWeatherMapTemperature), $weatherTemperature);
    }
}