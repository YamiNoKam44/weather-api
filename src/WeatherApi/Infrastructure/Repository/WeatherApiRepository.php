<?php
declare(strict_types=1);

namespace App\WeatherApi\Infrastructure\Repository;


use App\WeatherApi\Domain\ValueObject\WeatherApi\RequestQueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WeatherApiRepository implements WeatherApiRepositoryInterface
{
    private const API_URL_BASE = 'http://api.weatherapi.com/v1/current.json?%s';

    private HttpClientInterface $httpClient;
    private string $apiKey;

    public function __construct(HttpClientInterface $httpClient, string $apiKey) {
        $this->httpClient = $httpClient;
        $this->apiKey = $apiKey;
    }

    public function fetchByCityName(string $city): float {
        $response = $this->httpClient->request(Request::METHOD_GET,
            sprintf(self::API_URL_BASE, RequestQueryBuilder::buildByCityQuery($city, $this->apiKey)));

        $result = json_decode($response->getContent(), true);

        return $result['current']['temp_c'];
    }

    /**
     * UNAVAILABLE
     */
    public function fetchByCityAndCountryName(string $city, string $country): float {
        return $this->fetchByCityName($city);
    }
}