<?php
declare(strict_types=1);

namespace App\WeatherApi\Infrastructure\Repository;

interface WeatherApiRepositoryInterface
{
    public function fetchByCityName(string $city): float;

    public function fetchByCityAndCountryName(string $city, string $country): float;
}