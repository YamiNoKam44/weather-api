<?php
declare(strict_types=1);

namespace App\WeatherApi\Infrastructure\Repository;

use App\WeatherApi\Domain\ValueObject\OpenWeatherMapApi\RequestQueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenWeatherMapApiRepository implements WeatherApiRepositoryInterface
{
    private const API_URL_BASE = 'https://api.openweathermap.org/data/2.5/weather?%s';

    private HttpClientInterface $httpClient;
    private string $apiKey;

    public function __construct(HttpClientInterface $httpClient, string $apiKey) {
        $this->httpClient = $httpClient;
        $this->apiKey = $apiKey;
    }

    public function fetchByCityName(string $city): float {
        $response = $this->httpClient->request(Request::METHOD_GET,
            sprintf(self::API_URL_BASE, RequestQueryBuilder::buildByCityQuery($city, $this->apiKey)));

        $result = json_decode($response->getContent(), true);

        return $result['main']['temp'];
    }

    public function fetchByCityAndCountryName(string $city, string $country): float {
        $response = $this->httpClient->request(Request::METHOD_GET,
            sprintf(self::API_URL_BASE, RequestQueryBuilder::buildByCityAndCountryQuery($city, $country, $this->apiKey)));

        $result = json_decode($response->getContent(), true);

        return $result['main']['temp'];
    }
}