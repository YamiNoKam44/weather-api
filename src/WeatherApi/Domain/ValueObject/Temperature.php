<?php
declare(strict_types=1);

namespace App\WeatherApi\Domain\ValueObject;


class Temperature
{
    private const KELVIN_VALUE_TO_CONVERT = 273.15;
    private float $temperature;

    public function __construct(float ...$temperatures) {
        $this->temperature = $this->calculateTemperature($temperatures);
    }

    public function getTemperature(): float {
        return $this->temperature;
    }

    public function toArray(): array {
        return ['temperature' => $this->temperature];
    }

    public function toJson(): string {
        return json_encode($this->toArray());
    }

    public static function calculateFromKelvinToCelsius(float $temperature): float {
        return round($temperature - self::KELVIN_VALUE_TO_CONVERT, 1);
    }

    private function calculateTemperature(array $temperatures): float {
        $temperatureCount = count($temperatures);

        $fullTemperature = 0;
        foreach ($temperatures as $temperature) {
            $fullTemperature += $temperature;
        }

        return round($fullTemperature / $temperatureCount, 1);
    }
}