<?php
declare(strict_types=1);

namespace App\WeatherApi\Domain\ValueObject\WeatherApi;


class RequestQueryBuilder
{
    private const QUERY_ATTRIBUTE = 'q=';
    private const API_KEY_ATTRIBUTE = '&key=';

    public static function buildByCityQuery(string $city, string $apiKey): string {
        return implode([self::QUERY_ATTRIBUTE, $city, self::API_KEY_ATTRIBUTE, $apiKey]);
    }
}