<?php
declare(strict_types=1);

namespace App\WeatherApi\Domain\ValueObject\OpenWeatherMapApi;


class RequestQueryBuilder
{
    private const QUERY_ATTRIBUTE = 'q=';
    private const API_KEY_ATTRIBUTE = '&appid=';

    public static function buildByCityQuery(string $city, string $apiKey): string {
        return implode([self::QUERY_ATTRIBUTE, $city, self::API_KEY_ATTRIBUTE, $apiKey]);
    }

    public static function buildByCityAndCountryQuery(string $city, string $country, string $apiKey): string {
        return implode([self::QUERY_ATTRIBUTE, implode(',', [$city, $country]), self::API_KEY_ATTRIBUTE, $apiKey]);
    }
}
