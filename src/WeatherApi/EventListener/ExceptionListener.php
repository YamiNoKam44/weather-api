<?php
declare(strict_types=1);


namespace App\WeatherApi\EventListener;


use App\WeatherApi\Exception\WeatherApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void {
        $exception = $event->getThrowable();

        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        if ($exception instanceof WeatherApiException) {
            $statusCode = $exception->getStatusCode();
        }

        $event->setResponse(new JsonResponse(['message' => $exception->getMessage()], $statusCode));
    }
}