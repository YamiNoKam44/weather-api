<?php
declare(strict_types=1);


namespace App\WeatherApi\Exception;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class WeatherApiException extends HttpException
{
    private const STATUS_MESSAGE = [
        Response::HTTP_UNAUTHORIZED => 'Wrong api key, check it or contact with page administrator',
        Response::HTTP_FORBIDDEN => 'Request limit',
        Response::HTTP_NOT_FOUND => 'Wrong city name or incorrect country',
        Response::HTTP_TOO_MANY_REQUESTS => 'Request limit'
    ];


    public function __construct(int $statusCode) {
        parent::__construct($statusCode, self::STATUS_MESSAGE[$statusCode] ?? 'Unknow Error');
    }
}